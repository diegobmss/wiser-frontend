## wiser-frontend

Aplicação front-end do desafio da Wiser

## Requisitos

- [Node](https://nodejs.org/en/) >= 14.15.3 (Dê preferência a uma versão **LTS**)
- [NPM](https://docs.npmjs.com/cli/npm) >= 6.14.9
- [Yarn](https://yarnpkg.com/lang/en/) >= 1.22.10

## Tech Stack

- [NextJS](https://nextjs.org/docs/getting-started)
  - [React](https://pt-br.reactjs.org/)
    - [React Hooks](https://pt-br.reactjs.org/docs/hooks-intro.html)
    - [Typescript](https://www.typescriptlang.org/)
    - [Styled Components](https://www.styled-components.com/)
    - [Redux](https://redux.js.org/)
    - [Redux Saga](https://redux-saga.js.org/)
    - [Axios](https://github.com/axios/axios)

## Instalação

Use o [yarn](https://yarnpkg.com/lang/en/) para gerenciamento de pacotes no repositório.

```bash
  $ yarn
```

## Desenvolvimento

Use o comando abaixo para executar a aplicação.

```
  $ yarn dev
```

## Usuários

A aplicação usa o serviço de API da [MockAPI](https://mockapi.io).

Para login, temos 6 usuários cadastrados na API.

```
[
  {
    "email": "Archibald63@yahoo.com",
    "password": "xkw1Tw1I2zIml3V",
  },
  {
    "email": "Tatum5@gmail.com",
    "password": "0Z54Rucdbb8GhXW",
  },
  {
    "email": "Nikko98@yahoo.com",
    "password": "JVoL7vUXlKzziPr",
  },
  {
    "email": "Tessie.Mertz11@yahoo.com",
    "password": "Cxfkrb5AJ8D02vm",
  },
  {
    "email": "Demetris_Klocko@gmail.com",
    "password": "BKV5mN0tKGPMphf",
  }
]
```

## Heroku

A aplicação está disponível também no Heroku: [https://wiser-frontend-2021.herokuapp.com](https://wiser-frontend-2021.herokuapp.com)
