import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useForm } from "react-hook-form";
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from "yup";
import { toast } from 'react-toastify';

import { UsersActions } from '../../store/ducks/users';

import Button from '../../components/structure/Button';
import Layout from '../../components/structure/Layout';
import Input from '../../components/form/Input';

import { REQUEST_RESOLVED, REQUEST_REJECTED} from '../../utils/constants/request';

import * as S from './styled';

const Home = (): JSX.Element => {
  const dispatch = useDispatch();
  const { auth } = useSelector((state) => state.users);

  useEffect(() => {
    if(auth.requestStatus === REQUEST_RESOLVED) {
      toast.success(auth.requestResponse);
    } else if(auth.requestStatus === REQUEST_REJECTED) {
      toast.error(auth.requestResponse);
    }
  }, [auth])

  const { register, handleSubmit, errors } = useForm({
    resolver: yupResolver(Yup.object().shape({
      email: Yup.string().email('Digite um e-mail válido;').required('Campo obrigatório;'),
      password: Yup.string().required('Campo obrigatório;')
    }))
  });

  const onSubmit = ({ email, password }) => {
    dispatch(UsersActions.requestAuth(email, password));
  };

  return (
    <Layout title="Login">
      <S.Flex>
        <S.Image />
        <S.Content>
          <S.Container>
            <S.Card>
              <S.Title>Olá, seja bem-vindo!</S.Title>
              <S.Description>Para acessar a plataforma, faça seu login.</S.Description>

              <S.Form onSubmit={handleSubmit(onSubmit)}>
                <Input label="E-mail" name="email" type="text" formRef={register} formErrors={errors} />
                <Input label="Senha" name="password" type="password" formRef={register} formErrors={errors} />

                <S.Button>
                  <Button type="submit" isLoading={false}>Entrar</Button>
                </S.Button>
              </S.Form>
            </S.Card>

            <S.ForgotPassword>Esqueceu seu login ou senha?<br />Clique <a href="">aqui</a></S.ForgotPassword>
          </S.Container>
        </S.Content>
      </S.Flex>
    </Layout>
  );
}

export default Home
