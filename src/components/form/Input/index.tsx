import * as S from './styled';

interface Input {
  label: string,
  name: string,
  type: string,
  formRef: any,
  formErrors: any,
}

const Input = ({ label, name, type, formRef, formErrors }: Input): JSX.Element => (
  <S.Input hasError={!!formErrors[name]}>
    {label && <label htmlFor={name}>{label}</label>}
    <input name={name} type={type} ref={formRef} />

    {formErrors[name] && <S.Error>{formErrors[name].message}</S.Error>}
  </S.Input>
);

export default Input
