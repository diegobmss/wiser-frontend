import { all } from 'redux-saga/effects';

import * as users from './users';

function* Sagas():any {
  yield all([
    users.watcherSaga(),
  ]);
}

export default Sagas;
