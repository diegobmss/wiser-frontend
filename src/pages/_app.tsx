import React from 'react';
import Head from 'next/head';

import { wrapper } from '../store/index';
import GlobalStyle from '../styles/globalStyle';

import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const App = ({ Component, pageProps }) => (
  <>
    <Head>
      <link
        href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;600&display=swap"
        rel="stylesheet"
      />
    </Head>
    <GlobalStyle />
    <ToastContainer />
    <Component {...pageProps} />
  </>
);

export default wrapper.withRedux(App);