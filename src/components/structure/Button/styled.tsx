import styled from 'styled-components'

export const Button = styled.button`
  background: linear-gradient(267.79deg, #383E71 0%, #9D25B0 99.18%);
  box-shadow: 0px 10px 25px #CF99DB;
  border-radius: 8px;
  width: 100%;
  height: 48px;
  font-weight: 600;
  font-size: 16px;
  line-height: 20px;
  color: #FFFFFF;
  text-transform: uppercase;
  border: none;

  &:hover {
    background: linear-gradient(267.79deg, #323555 0%, #8f309e 99.18%);
  }

  @media (max-width: 767px) {
    max-width: 168px;
    box-shadow: none;
  }
`;