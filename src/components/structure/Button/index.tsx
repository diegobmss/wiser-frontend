import * as S from './styled';

interface Button {
  type: string,
  children: string,
  isLoading: boolean,
}

const Button = ({ type, children, isLoading }: Button): JSX.Element => (
  <S.Button type={type}>
    {isLoading ? 'Carregando...' : children}
  </S.Button>
);

export default Button
