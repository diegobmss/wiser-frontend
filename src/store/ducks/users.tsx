import { createActions, createReducer } from 'reduxsauce';
import {
  REQUEST_NOT_STARTED,
  REQUEST_PENDING,
  REQUEST_REJECTED,
  REQUEST_RESOLVED,
} from '../../utils/constants/request';

export const { Types: UsersTypes, Creators: UsersActions } = createActions({
  requestAuth: ['email', 'password'],
  requestAuthSuccess: ['response'],
  requestAuthFailure: ['response'],
});

const INITIAL_STATE = {
  auth: {
    requestStatus: REQUEST_NOT_STARTED,
    requestResponse: {},
  },
};

const requestAuth = (state) => ({
  ...state,
  auth: {
    requestStatus: REQUEST_PENDING,
  },
});

const requestAuthSuccess = (state, { response }) => ({
  ...state,
  auth: {
    requestResponse: response,
    requestStatus: REQUEST_RESOLVED,
  },
});

const requestAuthFailure = (state, { response }) => ({
  ...state,
  auth: {
    requestResponse: response,
    requestStatus: REQUEST_REJECTED,
  },
});

export default createReducer(INITIAL_STATE, {
  [UsersTypes.REQUEST_AUTH]: requestAuth,
  [UsersTypes.REQUEST_AUTH_SUCCESS]: requestAuthSuccess,
  [UsersTypes.REQUEST_AUTH_FAILURE]: requestAuthFailure,
});
