/* eslint-disable no-unused-vars */
import { put, takeLatest } from 'redux-saga/effects';
import { UsersTypes, UsersActions } from '../../store/ducks/users';
import { api } from '../../services/api';
import { GET } from '../../utils/constants/verbs';

function* requestAuth({ email, password }: any) {
  try {
    const response = yield api({
      method: GET,
      url: `/users`,
    });

    const findUser = response?.data.find((x) => x.email === email && x.password === password);

    if(findUser) {
      yield put(UsersActions.requestAuthSuccess(`Olá, ${findUser.name}. Você está autenticado. :)`));
    } else {
      yield put(UsersActions.requestAuthFailure('Usuário ou senha incorretos.'));
    }
  } catch ({ response }) {
    yield put(UsersActions.requestAuthFailure(`Aconteceu algum erro na API: ${response.status} | ${response.data}`));
  }
}

export function* watcherSaga():any {
  yield takeLatest(UsersTypes.REQUEST_AUTH, requestAuth);
}
