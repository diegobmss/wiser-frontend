import Head from 'next/head'

interface Layout {
  title: string,
  children: JSX.Element,
}

const Layout = ({ title, children }: Layout): JSX.Element => (
  <>
    <Head>
      <title>{`Wiser | ${title}`}</title>
      <link rel="icon" href="/favicon.ico" />
    </Head>

    {children}
  </>
)

export default Layout
