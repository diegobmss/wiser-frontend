import axios from 'axios'

const headers = {
  'Content-Type': 'application/json',
}

export const api = axios.create({
  baseURL: 'https://60f4c55e2208920017f39dd2.mockapi.io/',
  headers,
})
