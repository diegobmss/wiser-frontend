import styled from 'styled-components'

export const Flex = styled.div`
  display: flex;
  align-items: center;

  @media (max-width: 767px) {
    display: block;
  }
`;

export const Image = styled.div`
  width: 50%;
  height: 100vh;
  background-image: linear-gradient(180deg, rgba(105, 57, 153, 0) 0%, #130525 100%), url('/images/bg.png');
  background-size: cover;
  background-position: center center;
  background-repeat: no-repeat;

  @media (max-width: 767px) {
    width: 100%;
  }
`;

export const Content = styled.div`
  width: 50%;
  padding: 0px 112px;

  @media (min-width: 768px) and (max-width: 991px) {
    padding: 0px 75px;
  }

  @media (max-width: 767px) {
    width: 100%;
    padding: 0px 32px;
  }
`;

export const Container = styled.div`
  max-width: 256px;

  @media (max-width: 767px) {
    max-width: 100%;
  }
`;

export const Card = styled.div`
  @media (max-width: 767px) {
    margin-top: -66vh;
    width: 100%;
    background: #FAF5FF;
    padding: 24px 28px 28px 28px;
    border-radius: 8px;
    height: calc(100% - 50px);
  }
`;

export const Title = styled.h1`
  max-width: 231px;
  margin-bottom: 16px;
  font-weight: 400;
  font-size: 40px;
  line-height: 48px;
  color: #383E71;

  @media (max-width: 767px) {
    max-width: 139px;
    font-size: 24px;
    line-height: 32px;
    text-align: center;
    margin: 0 auto 16px auto;
  }
`;

export const Description = styled.h2`
  max-width: 222px;
  margin-bottom: 43px;
  font-weight: 600;
  font-size: 16px;
  line-height: 20px;
  color: #989FDB;

  @media (max-width: 767px) {
    max-width: 255px;
    font-size: 12px;
    line-height: 20px;
    text-align: center;
    margin: 0 auto 20px auto;
  }
`;

export const Form = styled.form`
  margin-bottom: 32px;
`;

export const Button = styled.div`
  margin-top: 24px;

  @media (max-width: 767px) {
    text-align: center;
    position: absolute;
    left: 0px;
    right: 0px;
  }
`;

export const ForgotPassword = styled.p`
  font-size: 14px;
  line-height: 20px;
  text-align: center;
  color: #989FDB;

  @media (max-width: 767px) {
    margin-top: 44px;
    margin-bottom: 40px;
    color: white;

    a {
      color: white;
    }
  }
`;