import styled from 'styled-components'

export const Input = styled.div`
  margin-bottom: 10px;

  label {
    display: block;
    font-size: 10px;
    line-height: 28px;
    color: #383E71;
    text-transform: uppercase;
    margin-left: 10px;
  }

  input {
    border: 1px solid;
    border-color: ${props => props.hasError ? '#FF377F' : '#989FDB'};
    box-sizing: border-box;
    border-radius: 8px;
    width: 100%;
    height: 48px;
    padding: 17px;
    color: #989FDB;
    background: transparent;
  }
`;

export const Error = styled.span`
  font-size: 10px;
  line-height: 28px;
  display: flex;
  align-items: center;
  color: #FF377F;
  margin-left: 10px;
`;