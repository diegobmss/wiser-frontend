import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga'
import { createWrapper } from 'next-redux-wrapper';

import ducks from './ducks';
import sagas from './sagas'

const bindMiddleware = (middleware) => {
  if (process.env.NODE_ENV !== 'production') {
    const { composeWithDevTools } = require('redux-devtools-extension');
    return composeWithDevTools(applyMiddleware(...middleware));
  }
  return applyMiddleware(...middleware);
};

export const makeStore = (context) => {
  const sagaMiddleware = createSagaMiddleware()
  const store:any = createStore(ducks, bindMiddleware([sagaMiddleware]))

  store.sagaTask = sagaMiddleware.run(sagas)

  return store
}

export const wrapper = createWrapper(makeStore)