import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`
  * {
    margin: 0;
    padding: 0;
    outline: 0;
    box-sizing: border-box;
    font-family: 'Montserrat', sans-serif;
  }

  *:focus {
    outline: 0;
  }

  body {
    background: #f9f3fe;
    -webkit-font-smoothing: antialiased;

    @media (max-width: 767px) {
      background: black;
    }
  }

  input {
    -webkit-appearance: none;
  }

  button {
    cursor: pointer;
  }

  .Toastify__toast {
    font-size: 15px;
    font-weight: bold;
  }
`;

export default GlobalStyle;